import torch
import torchvision


class ConvBlock(torch.nn.Module):
    """Convolution block of the UNet"""

    def __init__(self, in_channels: int, out_channels: int) -> None:
        super().__init__()
        self.conv_block = torch.nn.Sequential(
            torch.nn.Conv2d(in_channels, out_channels, kernel_size=3, bias=False),
            torch.nn.BatchNorm2d(out_channels),
            torch.nn.ReLU(inplace=True),
            torch.nn.Conv2d(out_channels, out_channels, kernel_size=3, bias=False),
            torch.nn.BatchNorm2d(out_channels),
            torch.nn.ReLU(inplace=True)
        )

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        return self.conv_block(x)


class DownConv(torch.nn.Module):
    """UNet downsampling block"""

    def __init__(self, in_channels: int, out_channels: int) -> None:
        super().__init__()
        self.down_conv = torch.nn.Sequential(
            torch.nn.MaxPool2d(kernel_size=2, stride=2),
            ConvBlock(in_channels, out_channels)
        )

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        return self.down_conv(x)


class UpConv(torch.nn.Module):
    """UNet upsampling block"""

    def __init__(self, in_channels: int, out_channels: int) -> None:
        super().__init__()
        self.up = torch.nn.Sequential(
            # torch.nn.Upsample(scale_factor=2, mode='bilinear'),
            torch.nn.ConvTranspose2d(in_channels, out_channels, kernel_size=2, stride=2)
        )
        self.conv_block = ConvBlock(2 * out_channels, out_channels)

    def forward(self, x: torch.Tensor, xi: torch.Tensor) -> torch.Tensor:
        x = self.up(x)
        xi = torchvision.transforms.CenterCrop(x.size()[2:4])(xi)
        x = torch.cat([xi, x], dim=1)
        return self.conv_block(x)


class UNet(torch.nn.Module):
    """Unet Model"""

    def __init__(self) -> None:
        super().__init__()
        self.input_conv_block = ConvBlock(3, 64)
        self.down_conv_1 = DownConv(64, 128)
        self.down_conv_2 = DownConv(128, 256)
        self.down_conv_3 = DownConv(256, 512)
        self.down_conv_4 = DownConv(512, 1024)

        self.up_conv_1 = UpConv(1024, 512)
        self.up_conv_2 = UpConv(512, 256)
        self.up_conv_3 = UpConv(256, 128)
        self.up_conv_4 = UpConv(128, 64)

        self.output_conv = torch.nn.Conv2d(in_channels=64, out_channels=3, kernel_size=1)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        x1 = self.input_conv_block(x)

        x2 = self.down_conv_1(x1)
        x3 = self.down_conv_2(x2)
        x4 = self.down_conv_3(x3)
        x5 = self.down_conv_4(x4)

        x = self.up_conv_1(x5, x4)
        x = self.up_conv_2(x, x3)
        x = self.up_conv_3(x, x2)
        x = self.up_conv_4(x, x1)

        return self.output_conv(x)
